const program = require('commander')
const ncp = require('ncp').ncp
const execa = require('execa')
const O = require('otag')
const inquirer = require('inquirer')
const fs = require('fs')
const path = require('path')

let Kurucu = require('./package.json')

let Kurulum = path.join(path.dirname(fs.realpathSync(__filename)), './Kur')
let Dürü = require(Kurulum + '/package.json')

let yol = process.env.PWD
yol += process.argv[2] ? '/' + process.argv[2] :''

program
  .version('1.0', '-s, --sürüm')
  .parse(process.argv)

const write = (f, d) => fs.writeFileSync(yol + '/' + f, d)
const read = f => fs.readFileSync(yol + '/' + f, 'utf-8')

const Ayar = async() => {
  console.log('Yeni bir ⊕ Otağ kuruluyor...')

  let {ad, açıklama, yeterge, iyelik} = await inquirer.prompt([
    {
      name: 'ad',
      message: 'Tasarı adı girin:',
      type: 'input'
    },
    {
      name: 'açıklama',
      message: 'Bu tasarı nedir ne iş yapacak?',
      type: 'input'
    },
    {
      name: 'iyelik',
      message: 'Tasarı iyesi(sahibi)/yürütücüsü kim?',
      type: 'input'
    },
    {
      name: 'yeterge',
      message: 'Hangi açık kaynak yetergesi(lisansı) ile dağıtılacak?',
      type: 'input'
    }
  ])
  const seçenekler = ['evet', 'hayır'].map((name,i) => ({name, value: !i }))

  const {onay} = await inquirer.prompt([
    {
      name: 'onay',
      message: `Bu bilgiler doğru mu?
        ad: ad₺
        açıklama: açıklama₺
        iyelik: iyelik₺
        yeterge: yeterge₺
      `.vars({ad, açıklama, iyelik, yeterge}),
      type: 'list',
      choices: seçenekler
    }])
  ncp.limit = 16
  onay && ncp(Kurulum, yol, async(error) => {
    if (error) {return console.error(error)}
    yeterge = yeterge.toUpperCase()
    Dürü.name = ad
      .replace(/[ ]/g,'.')
      .toLowerCase()
      .replaceAll(
        [' ','ı','ç','ğ','ö','ş','ü'],
        ['.','i','c','g','o','s','u'])

    Dürü.description = açıklama
    Dürü.author = iyelik
    Dürü.license = yeterge
    write('package.json', JSON.stringify(Dürü, null, 1))

    write('README.MD',
      read('README.MD')
        .vars({
          ad,
          açıklama,
          iyelik,
          yeterge,
          yıl: (new Date()).getFullYear()
        }))
    write('bet/ana.js',
      read('bet/ana.js')
        .vars({ad, açıklama}))
    write('bet/ek/iyelik.js',
      read('bet/ek/iyelik.js')
        .vars({iyelik}))
    write('bet/ek/yeterge.js',
      read('bet/ek/yeterge.js')
        .vars({yeterge}))
    await execa('npm', ['i', 'otag', '--save'], {stdio: 'inherit', cwd: yol})
    await execa('git', ['init'], {stdio: 'ignore', cwd: yol})
    await execa('git', ['add', '--all'], {stdio: 'inherit', cwd: yol})
    let Kurulum = require(yol+'/package.json')
    await execa('git', ['commit', `-m Otağ Kurulum\n    otag.kur : ${Kurucu.version}\n    otag     : ${Kurulum.dependencies.otag.substring(1)}`], {stdio: 'ignore', cwd: yol})
    console.clear()
    console.log(
      fs.readFileSync(
        path
          .join(
            path.dirname(
              fs.realpathSync(__filename)),
              './betik/tamamla'),'utf-8')
      .vars({ad,
        kur: Kurucu.version,
        otag: Kurulum.dependencies.otag.substring(1)
      }))

    let {tamamla} = await inquirer
      .prompt([
        {
          name: 'tamamla',
          message: 'Çalıştır ve tamamla?',
          type: 'list',
          choices: seçenekler
        }])

    if(tamamla){
      console.log('Yardımcı araçlar kuruluyor: `npm i`')
      await execa('npm', ['i'], {stdio: 'inherit', cwd: yol})
      console.log('Tasarı kurma işlemi: `npm run kur`')
      await execa('npm', ['run','kur'], {stdio: 'inherit', cwd: yol})
      console.log('Kurulumunuz tamamlandı. dizin: '+yol)
    }
  })
}
Ayar()