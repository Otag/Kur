import {O, Page} from 'otag'

import ana from '₺/ana.js'
import yasal from '₺/yasal.js'

import Kalıplar from '@/Kalıplar.js'

O.define('Model', Kalıplar)

let Kabuk = 'Kabuk'.kur({
  View: {
    yönlendirme: '#yönlendirme',
    taşıyıcı: 'Taşıyıcı'
  },
  template: `
    header
      .alan
        yönlendirme
    #gövde
      .alan
        taşıyıcı
    footer
      a[href="/#/yasal"]:Yasal`
})

let routes = {
  index: 'ana',
  ana,
  yasal
}

let Uygu = new Page({routes, handler: Kabuk.V('taşıyıcı')})
let Yönlendirme = Uygu.Navigation({hide: ['yasal']})

// Yerleştir
Yönlendirme.to = Kabuk.V('yönlendirme')
Kabuk.to = 'body'