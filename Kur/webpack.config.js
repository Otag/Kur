const path = require('path')
const webpack = require('webpack')
const UglifyJsPlugin = require('uglifyjs-webpack-plugin')

function resolve(dir) {
  return path.join(__dirname, './', dir)
}

module.exports = {
  entry: './uygu.js',
  output: {
    path: resolve('yay'),
    publicPath: './yay',
    filename: 'uygucuk.js'
  },
  resolve: {
    alias: {
      '@': resolve('/'),
      '₺': resolve('/bet/')
    },
    extensions: ['*', '.js']
  },
  devServer: {
    historyApiFallback: true,
    overlay: true
  },
  performance: false,
  devtool: '#eval-source-map'
}

if (process.env.NODE_ENV === 'production') {
  let plugins = (module.exports.plugins || []).concat([
    new webpack.DefinePlugin({
      'process.env': {
        NODE_ENV: '"production"'
      }
    }),
    new webpack.LoaderOptionsPlugin({
      minimize: true
    })
  ])
  Object.assign(module.exports, {
    optimization: {
      minimizer: [
        new UglifyJsPlugin({
          cache: true,
          sourceMap: true,
          uglifyOptions: {
            mangle: true,
            ecma: 6,
            compress: false
          },
          parallel: true
        })
      ]
    },
    devtool: '#source-map',
    plugins
  })
}